package Model;

import java.util.HashMap;
import java.util.Map;

public class Dictionary {

    private Map<String, Entry> entries;

    public Dictionary(){
        entries = new HashMap<>();
    }

    public void add(Entry e){
        entries.put(e.getWord(), e);
    }

    public Entry search(String word){
        return entries.get(word);
    }

    public void delete(String word){
            entries.remove(word);
    }


    public Map<String, Entry> getEntries() {
        return entries;
    }
}
