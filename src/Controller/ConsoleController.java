package Controller;


import Model.Dictionary;
import Model.Entry;

import java.nio.charset.spi.CharsetProvider;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class ConsoleController {
    private Map<String, Entry> sortedMap;
    private Dictionary dictionary;
    private Scanner scanner;

    public void start(){

        scanner = new Scanner(System.in);
        dictionary = new Dictionary();
        boolean isRunning = true;
        do {
            Choice ch = showMenu();
            switch (ch){
                case ADD:
                    addEntry();
                    break;
                case SEARCH:
                    search();
                    break;
                case DELETE:
                    delete();
                    break;
                case SORT:
                    sort();
                    break;
                case EXIT:
                    isRunning = false;
                    break;
                case INVALID:
                    System.out.println("Invalid data");
            }
        }while (isRunning);
        System.out.println("Thank you for using dictionary!");
    }

    private void sort(){
        sortedMap = new TreeMap<>(dictionary.getEntries());
        System.out.println(sortedMap.keySet());
    }

    private void delete(){
        System.out.println("Word for deleting");
        String word = scanner.nextLine();
        Entry e = dictionary.search(word);
        if (e != null){
            dictionary.delete(e.getWord());
        }else {
            System.out.println("No such word");
        }

    }

    private void search(){
        System.out.println("Search word: ");
        String word = scanner.nextLine();
        Entry e = dictionary.search(word);
        if (e != null){
            System.out.println("Translation is: " + e.getTranslation());
        }else {
            System.out.println("No such word");
        }

    }

    private void addEntry(){
        System.out.println("Please write word");
        String word = scanner.nextLine();
        System.out.println("Please write translation");
        String translation = scanner.nextLine();
        System.out.println("Please write transcription");
        String transcription = scanner.nextLine();
        Entry e = new Entry(word, translation, transcription);
        dictionary.add(e);
    }

    private Choice showMenu(){
        System.out.println("---MENU---");
        System.out.println("1. ADD");
        System.out.println("2. SEARCH");
        System.out.println("3. DELETE");
        System.out.println("4. SORT");
        System.out.println("5. EXIT");

        int choise = Integer.parseInt(scanner.nextLine());
        switch (choise){
            case 1:
                return Choice.ADD;
            case 2:
                return Choice.SEARCH;
            case 3:
                return Choice.DELETE;
            case 4:
                return Choice.SORT;
            case 5:
                return Choice.EXIT;
            default:
                return Choice.INVALID;
        }


    }

    private enum Choice{
        ADD,
        SEARCH,
        DELETE,
        SORT,
        EXIT,
        INVALID,

    }

}
